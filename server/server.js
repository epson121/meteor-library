  Meteor.startup(function () {
    // Books.remove({});
  });

  Meteor.publish("books", function () {

    return Books.find();

  });

  Meteor.publish("users", function() {
    return Meteor.users.find();
  });

  Meteor.methods({
    // saveFile1 : function(file, name) {
    //     var fs = Npm.require('fs');
    //     var path = Npm.require('path');
    //     pub = "public" + path.sep + name;
    //     p = path.join(process.env.PWD, pub);
    //     // console.log(path + name);
    //     fs.writeFile(p, file, 'binary', function(err) {
    //           if (err) {
    //               console.log(err);
    //               throw (new Meteor.Error(500, 'Failed to save file.', err));
    //           } else {
    //               console.log('The file ' + name + ' was saved to ' + p);
    //           }
    //       });
    //     return name;
    // },

    saveImage: function (buffer, bId) {
      Files.insert({data: buffer, bookId: bId});
    },

    sendLogMessage: function(){
        console.log("Hello world");
    },

    saveBook: function(book) {
      Books.insert(book, function(error, result) {
            if (error) {
              console.log("error while adding book.");
            } else {
              return result;
            }
        });
    },

    updateUserStatus: function(setAsAdmin, id) {
      Meteor.users.update({_id: id}, {$set : {profile: {admin: setAsAdmin}}}, function(err, res) {
          if (err) {
            console.log(err.reason);
          } else {
            console.log(res);
          }
        });
      }
   });

