Router.configure({
  layoutTemplate: 'main'
})

// Router.route('/', function() {
//     this.render('home');
// }, {
//     name: 'home'
// });

Router.route('/', {
  // this template will be rendered until the subscriptions are ready
  loadingTemplate: 'loading',
  name: 'home',

  // waitOn: function () {
  //   // return one handle, a function, or an array
  //   return Meteor.subscribe('books');
  // },
  data: function() {
    data = {book: Books.find({available: true})};
    return data;
  },

  waitOn: function() {
    return Meteor.subscribe('books');
  },

  action: function () {
    this.render('home');
  }
});

Router.route('/admin/users', {
  name: 'adminUsers',
  loadingTemplate: 'loading',
  data: function() {
    data = {user: Meteor.users.find().fetch()};
    console.log(Meteor.users.find().fetch());
    return data;
  },

  waitOn: function() {
    return Meteor.subscribe('users');
  },

  action: function() {
    this.render('admin_users');
  }

});

Router.route('/admin', function() {
    this.render('admin');
}, {
    name: 'admin'
});

Router.route('/admin/books', function() {
    this.render('admin_books');
  }, {
    name: 'adminBooks'
  }
)

Router.route('/register', function() {
    this.render('register');
}, {
    name: 'register',
});

Router.route('/login', function() {
    this.render('login');
}, {
    name: 'login'
});

Router.route('/addBook', function() {
    this.render('addBookItem');
}, {
    name: 'addBook'
});

Router.route('/book/:_id', function() {
    this.render('bookPage', {
      data: function() {
        return Books.findOne({_id: this.params._id});
      }
    })
}, {
    name: 'bookPage'
});

Router.onBeforeAction(function(pause) {
  console.log("On before");
  Messages.remove({seen: true});
  this.next();
});

Router.onBeforeAction(function(pause) {
  if (!Meteor.user() || !Meteor.user().profile || !Meteor.user().profile.admin) {
    this.redirect('/login');
  } else {
    this.next();
  }
}, {
  only: ['admin', 'adminBooks', 'adminUsers']
  // or except: ['routeOne', 'routeTwo']
});

if(Meteor.isClient){

  addMessage = function(msg) {
    Messages.insert({
      body: msg,
      date: new Date(),
      seen: false
    });
  }

  Template.bookList.helpers({
      // book: function () {
      //     return Books.find({available: true});
      // }
  });

  Template.adminBookList.helpers({
      book: function () {
          return Books.find({});
      }
  });

  Template.adminBookItem.helpers({
    available: function () {
      console.log(this.available);
      return this.available ? "Yes" : "No";
    },

    checked: function() {
      console.log("checked " + this.available);
      return this.available ? "checked" : "";
    }
  });

  Template.adminUserItem.helpers({
    isAdmin: function() {
      if (this.profile) {
        return this.profile.admin ? "checked" : "";
      }
      return "";
    }
  });

  Template.adminUserList.events({
    'click .toggleAdmin': function (event) {
      var elem = $(event.target).is(':checked');
      console.log(this._id);
      console.log(Meteor.user()._id);
      if (Meteor.user()._id == this._id){
        console.log("no no amigo");
        return;
      }
      console.log(elem);
        Meteor.call('updateUserStatus', elem, this._id, function (error, result) {

        });

    }
  });

  Template.bookItem.helpers({
    available: function () {
      // console.log(this.available);
      return this.available ? "Yes" : "No";
    }
  });

  Template.addBookItem.events({
      'submit form': function (event) {
          event.preventDefault();

        var bookName = $('[name=bookName]').val();
        var bookDesc = $('[name=bookDescription]').val();
        var bookImage = $('[name=bookImage]');
        var file = (bookImage[0]).files[0];
        var bookAvailable = $('[name=bookAvailable]').prop('checked');
        var reader = new FileReader();
        var imgPath = null;
        if (file) {
          Images.insert(file, function (err, fileObj) {
            if (err){
               console.log(err.reason);
            } else {
               // handle success depending what you need to do
              var bookId = "/cfs/files/images/" + fileObj._id;
               Books.insert({
                    name: bookName,
                    description: bookDesc,
                    path: bookId,
                    available: bookAvailable
                }, function(error, result) {
                    if (error) {
                      console.log("added");
                      addMessage(error.reason);
                    } else {
                      addMessage("Book " + bookName + " has been added to library.");
                      Router.go('home');
                    }
                });
            }
          });
        } else {
          Books.insert({
              name: bookName,
              description: bookDesc,
              available: bookAvailable
          }, function(error, result) {
              if (error) {
                console.log("added");
                addMessage(error.reason);
              } else {
                addMessage("Book " + bookName + " has been added to library.");
                Router.go('home');
              }
          });
        }
      }
  });

  Template.adminBookItem.events({
      'submit form': function (event) {
          event.preventDefault();
        var form = $(event.target);
        var bookName = form.find('[name=bookName]').val();
        var bookDesc = form.find('[name=bookDescription]').val();
        var bookImage = form.find('[name=bookImage]');
        var file = (bookImage[0]).files[0];
        var bookAvailable = form.find('[name=bookAvailable]').prop('checked');
        var self = this;
        if (file) {
          console.log(file);
          Images.insert(file, function (err, fileObj) {
            if (err){
               console.log(err.reason);
            } else {
               // handle success depending what you need to do
              console.log("successfully added a book");
              var bookId = "/cfs/files/images/" + fileObj._id;
              console.log("Book id" + bookId);
              Books.update({_id: self._id},
                          { $set : {
                            name: bookName,
                            description: bookDesc,
                            available: bookAvailable,
                            path: bookId
                          }
                }, function(error, result) {
                          if (error) {
                            console.log("added");
                            addMessage(error.reason);
                          } else {
                            addMessage("Book " + bookName + " has been updated");
                            Router.go('home');
                          }
                  });
            }
          });
        } else {
          Books.update({_id: this._id},
            { $set : {
              name: bookName,
              description: bookDesc,
              available: bookAvailable,
            }
              }, function(error, result) {
                if (error) {
                  // console.log("added");
                  addMessage(error.reason);
                } else {
                  addMessage("Book " + bookName + " has been updated to library.");
                  // Router.go('home');
                }
          });
        }
        return;
      }
  });

  Template.register.events({
    'submit form': function(event){
        event.preventDefault();
        var em = $('[name=email]').val();
        var pass = $('[name=password]').val();
        var options = {
            email: em,
            password: pass,
            profile: {
                admin: true
            },
        };
        Accounts.createUser(options, function(error) {
          if (error) {
            console.log(error.reason);
            Messages.insert({
              body: "Error registering."
            });
          } else {
            Messages.insert({
              body: "You've successfully registered."
            });
            Router.go('home');
          };
        });
      }
    });

  Template.login.events({
    'submit form': function(event){
        event.preventDefault();
        Messages.remove({});
        var email = $('[name=email]').val();
        var password = $('[name=password]').val();
        Meteor.loginWithPassword(email, password, function(error) {
          if (error) {
            console.log(error.reason);
             Messages.insert({
              body: error.reason,
              date: new Date(),
              seen: false
            });
          } else {
            Router.go('home');
          }
        });
      }
    });

  Template.header.helpers({
    isAdmin: function (user) {
      if (user){
        return user.profile.admin ? true : false;
      }
    }
  });

  Template.header.events({
    'click .logout': function(event) {
      event.preventDefault();
      Meteor.logout();
      Messages.insert({
        body: "You've successfully logged out."
      });
      Router.go('login');
    }
  });

  Template.messages.helpers({
      messages: function() {
        return Messages.find();
      },
  });

  Template.message.onRendered(function () {
    var error = this.data;
    console.log("on rendered");
    console.log(error);
    Messages.remove({seen: true});
    Meteor.setTimeout(function() {
        console.log("called set timeout");
        if (!error) {
          return;
        }
        console.log(error._id);
        Messages.update({_id: error._id}, {$set: {seen: true}});
      }, 100);
  });

  Template.messages.onRendered(function() {
    console.log("Re rendering messages template");
  });

  Template.message.events({
    'click #delete_message': function (event) {
      Messages.remove(this);
    }
  });

}