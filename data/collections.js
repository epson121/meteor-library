Books = new Meteor.Collection('books');
Messages = new Meteor.Collection(null);

var imageStore = new FS.Store.GridFS("images");

Images = new FS.Collection("images", {
 stores: [imageStore]
});

Files = new Meteor.Collection("files");